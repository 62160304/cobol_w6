       IDENTIFICATION DIVISION. 
       PROGRAM-ID. write-grade1.
       AUTHOR. SIRAPATSON.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "grade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD SCORE-FILE.
       01 SCORE-DETAIL.
           05 STU-ID PIC X(8).
           05 MIDTERM-SCORE PIC 9(2)V9(2).
           05 FINAL-SCORE PIC 9(2)V9(2).
           05 PROJECT-SCROE PIC 9(2)V9(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE 
           MOVE "62160313" TO STU-ID
           MOVE "40.00" TO MIDTERM-SCORE
           MOVE "50.00" TO FINAL-SCORE 
           MOVE "10.00" TO PROJECT-SCROE
           WRITE SCORE-DETAIL 

           MOVE "62160312" TO STU-ID
           MOVE "30.00" TO MIDTERM-SCORE
           MOVE "40.00" TO FINAL-SCORE 
           MOVE "10.00" TO PROJECT-SCROE
           WRITE SCORE-DETAIL 

           MOVE "62160314" TO STU-ID
           MOVE "40.00" TO MIDTERM-SCORE
           MOVE "50.00" TO FINAL-SCORE 
           MOVE "10.00" TO PROJECT-SCROE
           WRITE SCORE-DETAIL 

           MOVE "62160315" TO STU-ID
           MOVE "20.00" TO MIDTERM-SCORE
           MOVE "50.00" TO FINAL-SCORE 
           MOVE "5.00" TO PROJECT-SCROE
           WRITE SCORE-DETAIL 

           CLOSE SCORE-FILE 


           GOBACK 
           .